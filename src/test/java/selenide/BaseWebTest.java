package selenide;


import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;

public class BaseWebTest {

    @Test
    public void simpleTest() {
        WebDriverManager.chromedriver().setup();
        Configuration.headless = true;
        Configuration.browser = "chrome";
        open("https://auto.ria.com/uk/");
        //ss

    }
}

