package selenide.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;

public class GooglePage {

    private SelenideElement searchField = $(byName("q"));

    private SelenideElement searchButton = $(byName("btnK"));

    public GooglePage navigateTo(String url) {
        Selenide.open(url);
        return this;
    }

    public GooglePage searchFacebook() {
        searchField.sendKeys("Facebook");

        return this;
    }

    public ResultOfSearch clickButton() {
        searchButton.submit();

        return new ResultOfSearch();
    }
}
