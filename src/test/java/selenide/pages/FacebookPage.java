/*
package selenide.pages;

import com.codeborne.selenide.SelenideElement;
import core.DriverManager;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class FacebookPage {

   */
/* private By emailField = By.id("email");
    private By passwordField = By.id("pass");
    private By buttonLogIn = By.xpath("//input[contains(@type,'submit')]");
    private By alertMessage = By.xpath("//div[contains(@role,'alert')]");*//*


    private SelenideElement emailField = $(byId("email"));
    private SelenideElement passwordField = $(byId("pass"));
    private SelenideElement buttonLogIn = $(byXpath("//input[contains(@type,'submit')]"));
    private SelenideElement alertMessage = $(byXpath("//div[contains(@role,'alert')]"));



    public FacebookPage navigateTo(String url) {
      DriverManager.getChromeDriver().get(url);

      return this;
  }

    public String isUrl(String url) {
        return  DriverManager.getChromeDriver().getCurrentUrl();
    }

    public FacebookPage enterWrongEmail(String login) {
        emailField.sendKeys(login);

        return new FacebookPage();
    }

    public FacebookPage enterWrongPassword(String password) {
        passwordField.sendKeys(password);

        return this;
    }

    public FacebookPage clickButtonLogIn() {
        buttonLogIn.click();

        return this;
    }

    public String identifyErrorMessage() {
       return alertMessage.getText();
    }


}
*/
